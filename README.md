# Eco heroes profile

Proyecto creado para realizar la vista profile de la app EcoHeroes.


status vista actual:

 <img src="image-versions/2020-07-26.png" width="450" height="812">


## TODO:

- [x] background color.
- [x] widget de imagen de usuario.
- [x] agregar custom fonts.
- [x] agregar custom icons.
- [x] modelo de datos profile.
- [x] servicio de obtención de datos profile(Fake).
- [x] widget de nombre de usuario e instagram.
- [x] widget de puntos.
- [x] widget de hojas.
- [x] widget de contribución total
- [x] widget de contribución unitaria.
- [x] widget de navegación profile.
- [ ] widget de navegación app.



