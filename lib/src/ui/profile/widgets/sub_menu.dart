import 'package:ecoheroes_profile/src/models/contribution.dart';
import 'package:ecoheroes_profile/src/models/profile.dart';
import 'package:ecoheroes_profile/src/resources/custom_icon_icons.dart';
import 'package:flutter/material.dart';

class SubMenu extends StatefulWidget {
  final Profile profile;

  SubMenu({Key key, @required this.profile}): super(key: key);
  @override
  _SubMenuState createState() => _SubMenuState();
}

class _SubMenuState extends State<SubMenu> {


  /* the content of this widget change with the buttons.
  * the options of content are the following:
  * 1 contribution container
  * 2 configuration container
  * 3 cupons container
  */

  int content = 1;
  List<Contribution> contributions = [];
  bool _isPawActivated = true;
  bool _isConfigActivated = false;
  bool _isCuponActivated = false;
  //style sizes
  final double topButtonsPadding = 16;
  final double bottomButtonsPadding = 14;
  final double bottomBigBoxPadding = 18;
  


  void setContent(int index) {
    setState(() {
      this.content = index;
      switch (index) {
        case 1:
          _isPawActivated = true;
          _isConfigActivated = false;
          _isCuponActivated = false;
          break;
        case 2:
          _isPawActivated = false;
          _isConfigActivated = true;
          _isCuponActivated = false;
          break;
        case 3:
          _isPawActivated = false;
          _isConfigActivated = false;
          _isCuponActivated = true;
          break;
        default:
          _isPawActivated = true;
          _isConfigActivated = false;
          _isCuponActivated = false;
          break;
      }
    });
  }

  @override
  void initState() {
    contributions = widget.profile.contributions;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: topButtonsPadding),
          _buttons(),
          SizedBox(height: bottomButtonsPadding),
          _content()
        ],
      )
    );
  }

  Widget _newButton(name, int index, bool activated) {
    return new GestureDetector(
      onTap: (){
        setContent(index);
      },
      child: new Container(
        width: 88,
        padding: EdgeInsets.only(left: 5, top: 3, right: 5, bottom: 3),
        decoration: BoxDecoration(
          border: Border.all(color: activated  ? Color.fromRGBO(0, 218, 170, 1.0) : Colors.transparent),
          borderRadius: BorderRadius.circular(10),
          shape: BoxShape.rectangle,
        ),
        child: Text(
          name,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12.0,
            color: activated  ? Color.fromRGBO(0, 218, 170, 1.0) : Colors.grey, 
          ),
        ),
      )
    );
  }
  
  Widget _buttons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _newButton("Mi Huella", 1, _isPawActivated),
        SizedBox(width: 10.0),
        _newButton("Configuración", 2, _isConfigActivated),
        SizedBox(width: 10.0),
        _newButton("Mis cupones", 3, _isCuponActivated),
      ],
    );
  }

  Widget _content() {
    switch (this.content) {
      case 1:
        return _pawContent();
      case 2:
        return _noContent();
      case 3:
        return _noContent();
      default:
        return _pawContent();
    }
  }

  Widget _smallBox(String title, String subTitle, IconData icon, Color color, String value) {
    return  Container(padding: EdgeInsets.only(left:5, right: 5, bottom: 5),
    child: DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: Colors.grey[200], blurRadius:5, spreadRadius: 2, offset: Offset(0, 4),),
        ],
      ),
      child: Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0),
            Text(title, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w800 , fontSize: 13)),
            Text(subTitle, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.normal, color: Colors.grey[600], fontSize: 12)),
            SizedBox(height: 10.0),
            Icon(icon, color: color, size: 40,),
            SizedBox(height: 8.0),
            Text(value, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w800, fontSize: 15)),
          ],
        ),
      ),
    ));
  }

  Widget _bigBox(String content) {
    return Container(
      margin: EdgeInsets.only(bottom: bottomBigBoxPadding, top: 5, left: 5, right: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: Colors.grey[200], blurRadius: 2, spreadRadius: 2, offset: Offset(0, 4),),
        ],
      ),
      height: 60,
      width: 300,
      child: Row(     
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(CustomIcon.recycle, color: Color.fromRGBO(166, 229, 95, 1), size: 30,),
          SizedBox(width: 20.0), 
          Text(
            content,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800)
          ),
        ],
      ),   
    );
  }

  Widget _finalBox() {
    return Container(
      margin: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey[350],),
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20)),
        color: Colors.white,),
      height: 50,
      width: 350,
      child: Row(     
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(CustomIcon.tasks, color: Colors.grey[350], size: 30,),
          SizedBox(width: 135), 
          Icon(CustomIcon.profile, color: Color.fromRGBO(0, 218, 170, 1.0), size: 30,),  
        ],
      ),   
    );
  }

  Widget _pawContent() {
    return Column(
      children: <Widget>[
        Container(
          height: 267.8,
          //height: 200,
          width: 350,
          padding: EdgeInsets.only(left: 10, right: 10),
          child: CustomScrollView(
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: _bigBox('Total Residuos Reciclados\n100Kg'),
              ),
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 0.8,
                  crossAxisCount: 3,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 8,
                ),
                delegate: SliverChildListDelegate(
                  List.generate(contributions.length, (idx) {
                    return _smallBox(
                      contributions[idx].title,
                      contributions[idx].subtitle,
                      contributions[idx].icon,
                      contributions[idx].color,
                      contributions[idx].value
                    );
                  })
                ),
              )
            ] 
          )
        ),
     ],
    );
  }

  Widget _noContent() {
    return Container(
      margin: const EdgeInsets.all(10.0),
      height: 50,
      width: 280,
      child: Row(     
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Proximamente...', textAlign: TextAlign.center, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800),),
        ],
      ),   
    );
  }
}
