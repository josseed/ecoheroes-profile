import 'package:ecoheroes_profile/src/models/profile.dart';
import 'package:ecoheroes_profile/src/resources/custom_icon_icons.dart';
import 'package:ecoheroes_profile/src/services/profile/profile_service.dart';
import 'package:ecoheroes_profile/src/ui/profile/widgets/sub_menu.dart';
import 'package:flutter/material.dart';


class ProfileUI extends StatefulWidget {
  ProfileUI({Key key}) : super(key: key);

  @override
  _ProfileUIState createState() => _ProfileUIState();
}

class _ProfileUIState extends State<ProfileUI> {

  final double radiusImage = 80.0;
  final double heigthImage = 150.0;
  final double widthUserImage = 150.0;
  final double barHeight = 50.0;
  SubMenu subMenu;
  ProfileService vehicleService = new ProfileService();
  Profile _profile;
  bool _isLoading = true;

  @override
  void initState() {
    vehicleService.getProfile(1).then((value) => {
      setState(() {
        _profile = value;
        _isLoading = false;
        subMenu = new SubMenu(profile: _profile);
      })
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(_profile.toJson());
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          _profileImage(context),
          _userName(),
          Container(
            padding: EdgeInsets.only(top: heigthImage*2.25),
            alignment: Alignment.center,
            child: subMenu,
          )
        ],
      )
    );
  }

  Widget _userImage() {
    return Container(
      width: widthUserImage,
      height: heigthImage,
      decoration: new BoxDecoration(
        color: const Color(0xff7c94b6),
        image: new DecorationImage(
          image: AssetImage('images/user-example.jpg'),
          fit: BoxFit.cover,
        ),
        borderRadius: new BorderRadius.all(new Radius.circular(radiusImage)),
      ),
    );
  }

  Widget _userName() {
    return Container(
      padding: EdgeInsets.only(top: heigthImage*2*0.85),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          _isLoading ? Text("loading") : Text(_profile.getFullName(), style: TextStyle(fontSize: 21),),
          SizedBox(height: 2.0),
          _isLoading ? Text("loading") : Text(_profile.instagramAccount, style: TextStyle(color: Colors.grey)),
          SizedBox(height: 15.0),
          _buttons(),
          
        ],
      )
    );
  }

  Widget _leafButton() {
    return SizedBox.fromSize(
      size: Size(120, 40), // button width and height
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0)),
        onPressed: () {},
        color: Color.fromRGBO(0, 218, 170, 1.0),
        textColor: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(CustomIcon.leaf),
            SizedBox(width: 2.0), 
            Text(_profile.credits.toString()),
          ],
        ),
      ),
    );
  }

  Widget _pointButton() {
    return SizedBox.fromSize(
      size: Size(120, 40), // button width and height
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0)),
        onPressed: () {},
        color: Color.fromRGBO(0, 218, 170, 1.0),
        textColor: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(_profile.points.toString()),
            SizedBox(width: 2,), // icon
            Text("Puntos"), // text
          ],
        ),
      ),
    );
  }

  Widget _buttons() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _pointButton(),
          SizedBox(width: 20,),
          _leafButton()
        ],
      )
    );
  }

  Widget _profileImage(BuildContext context) {
    
    final double statusbarHeight = MediaQuery
        .of(context)
        .padding
        .top;
    final size = MediaQuery.of(context).size;

    final backgroundHeaderStyle = Container(
      padding: EdgeInsets.only(top: statusbarHeight),
      height: (size.height * 0.2) + barHeight/2,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0)),
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            Color.fromRGBO(0, 218, 170, 1.0),
            Color.fromRGBO(0, 217, 194, 1.0),
          ]
        )
      ),
    );

    var positionLeft = (size.width*0.5) - (heigthImage/2);
    var posititionTop = (size.height*0.2) + (barHeight/2) - (widthUserImage/2);

    return Stack(
      children: <Widget>[
        backgroundHeaderStyle,
        Positioned(top: posititionTop, left: positionLeft, child: _userImage()),
        Container(
          padding: EdgeInsets.only(top: 35.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 10.0, width: double.infinity),
              Image(
                image: AssetImage('images/logo.png'),
                width: widthUserImage
              ),
            ],
          ),
        )
      ],
    );
  }
}
