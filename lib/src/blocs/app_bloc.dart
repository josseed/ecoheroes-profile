
import 'package:ecoheroes_profile/src/blocs/profile/profile_bloc.dart';

class AppBloc {
  ProfileBloc _profileBloc;

  AppBloc() {
    _profileBloc = ProfileBloc();

  }

  ProfileBloc get profileBloc => _profileBloc;
}