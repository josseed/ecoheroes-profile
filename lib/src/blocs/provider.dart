import 'package:flutter/material.dart';
import 'app_bloc.dart';


class Provider extends InheritedWidget {

  final AppBloc bloc;

  static Provider _instancia;

  factory Provider({ Key key, Widget child, AppBloc bloc }) {
    if (_instancia == null) {
      _instancia = new Provider._internal(key: key, child: child, bloc: bloc);
    }
    return _instancia;
  }

  Provider._internal({ Key key, Widget child, this.bloc })
    : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;


  static AppBloc of (BuildContext context) =>
      (context.dependOnInheritedWidgetOfExactType<Provider>()).bloc;

}