
import 'package:ecoheroes_profile/src/blocs/profile/validators.dart';
import 'package:ecoheroes_profile/src/services/profile/profile_service.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBloc with Validators {

  final _nameController = new BehaviorSubject<String>();
  final _lastnameController = new BehaviorSubject<String>();
  final _runController = new BehaviorSubject<String>();
  final _addressController = new BehaviorSubject<String>();
  final _profileProvider = new ProfileService();
  
  Stream<String> get name => _nameController.stream.transform(validateName);
  Stream<String> get lastname => _lastnameController.stream.transform(validateLastname);
  Stream<String> get run => _runController.stream.transform(validateRun);
  Stream<String> get address => _addressController.stream.transform(validateAddress);
  
  Stream<bool> get valid => Rx.combineLatest4(name, lastname, run, address, (name, lastname, run, address) => true);

  Function(String) get changeName => _nameController.sink.add;
  Function(String) get changeLastname => _lastnameController.sink.add;
  Function(String) get changeRun => _runController.sink.add;
  Function(String) get changeAddress => _addressController.sink.add;
  
  editProfile() async {
    print("${_nameController.value}, ${_lastnameController.value}, ${_runController.value}, ${_addressController.value}");
  }

  dispose() {
    _nameController?.close();
    _lastnameController?.close();
    _addressController?.close();
  }
}