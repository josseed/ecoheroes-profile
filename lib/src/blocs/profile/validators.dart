import 'dart:async';

class Validators {

  final validateName = StreamTransformer<String, String>.fromHandlers(
    handleData: (name, sink) {
      Pattern pattern = r'^[a-zA-Z\s]*$';
      RegExp regExp = new RegExp(pattern);
      if (regExp.hasMatch (name)) {
        sink.add(name);
      } else {
        sink.addError('El nombre no es valido.');
      }
    }
  );

  final validateLastname = StreamTransformer<String, String>.fromHandlers(
    handleData: (lastname, sink) {
      Pattern pattern = r'^[a-zA-Z\s]*$';
      RegExp regExp = new RegExp(pattern);
      if (regExp.hasMatch (lastname)) {
        sink.add(lastname);
      } else {
        sink.addError('El apellido no es valido.');
      }
    }
  );

  final validateRun = StreamTransformer<String, String>.fromHandlers(
    handleData: (run, sink) {
      Pattern pattern = r'^[a-zA-Z\s]*$';
      RegExp regExp = new RegExp(pattern);
      if (regExp.hasMatch (run)) {
        sink.add(run);
      } else {
        sink.addError('El rut no es valido.');
      }
    }
  );

  final validateAddress = StreamTransformer<String, String>.fromHandlers(
    handleData: (address, sink) {
      Pattern pattern = r'^[a-zA-Z\s]*$';
      RegExp regExp = new RegExp(pattern);
      if (regExp.hasMatch (address)) {
        sink.add(address);
      } else {
        sink.addError('La dirección no es valida.');
      }
    }
  );

}