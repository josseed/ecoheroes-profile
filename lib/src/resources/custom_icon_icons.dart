import 'package:flutter/widgets.dart';

class CustomIcon {
  CustomIcon._();

  static const _kFontFam = 'CustomIcon';
  static const _kFontPkg = null;

  static const IconData discount = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData tree_filled = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData faq = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData profile = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData tasks = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData leaf = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData coupons = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData support = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData check_outline_circle = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData organic = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData plastic = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData recycle = IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData water = IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData co2 = IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData ecobrick = IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData tree_1 = IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
