import 'dart:convert';
import 'dart:ui';
import 'package:ecoheroes_profile/src/models/contribution.dart';
import 'package:ecoheroes_profile/src/models/profile.dart';
import 'package:ecoheroes_profile/src/resources/custom_icon_icons.dart';
import 'package:http/http.dart' as http;
class ProfileService {
  // TO DO.. Environment
  final String _baseUrl = 'https://localhost:1313/';
  final String _prefix = 'users';

  Future<dynamic> updateProfile(Profile profile) async {
    final url = '$_baseUrl/$_prefix/${profile.userId}/profile';
    final res = await http.patch(url, body: profile.toJson());
    final decodedData = json.decode(res.body);

    return null;
  }

  Future<Profile> getProfile(int id) async {
    // final url = '$_baseUrl/$_prefix/${id}/profile';
    // final res = await http.get(url);
    // final decodedData = json.decode(res.body);             
    List<Contribution> contributions = new List<Contribution>();
    contributions.add(new Contribution(
      title: "Plástico",
      subtitle: "Reciclado",
      icon: CustomIcon.plastic,
      color: Color.fromRGBO(253, 189, 67, 1),
      value: "10 Kg"
    ));
    contributions.add(new Contribution(
      title: "Agua",
      subtitle: "Ahorrada",
      icon: CustomIcon.water,
      color: Color.fromRGBO(100, 211, 255, 1),
      value: "20 Lts"
    ));
    contributions.add(new Contribution(
      title: "Residuos",
      subtitle: "Orgánicos",
      icon: CustomIcon.organic,
      color: Color.fromRGBO(166, 229, 95, 1),
      value: "10 Kg"
    ));
    contributions.add(new Contribution(
      title: "Árboles",
      subtitle: "Plantados",
      icon: CustomIcon.tree_1,
      color: Color.fromRGBO(166, 229, 95, 1),
      value: "3 Árboles"
    ));
    contributions.add(new Contribution(
      title: "CO2",
      subtitle: "Reducido",
      icon: CustomIcon.co2,
      color: Color.fromRGBO(156, 156, 156, 1),
      value: "2 Kg"
    ));
    contributions.add(new Contribution(
      title: "Ecoladrillos",
      subtitle: "Creados",
      icon: CustomIcon.ecobrick,
      color: Color.fromRGBO(253, 189, 67, 1),
      value: "5 Uds"
    ));
    Profile profile = new Profile(
      id: id,
      name: "José",
      lastname: "Zúñiga",
      instagramAccount: "@josseed",
      points: 150,
      credits: 100,
      contributions: contributions
    );
    return profile;
  }
}
