import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
    primaryColor: Colors.white,
    accentColor: Colors.white,
    hintColor: Colors.blue,
    dividerColor: Colors.red,
    buttonColor: Colors.blueAccent[100],
    scaffoldBackgroundColor: Color.fromRGBO(251, 251, 251, 1),
    canvasColor: Colors.purple,
    fontFamily: 'ProximaNova',
  );
}