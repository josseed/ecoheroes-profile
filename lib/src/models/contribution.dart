import 'dart:ui';
import 'package:flutter/cupertino.dart';

class Contribution {
  String title;
  String subtitle;
  IconData icon;
  Color color;
  String value;
  
  Contribution({
    this.title,
    this.subtitle,
    this.icon,
    this.color,
    this.value
  });

}
