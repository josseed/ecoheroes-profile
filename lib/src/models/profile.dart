import 'package:ecoheroes_profile/src/models/contribution.dart';

class Profile {
  int id;
  int userId;
  DateTime createdAt;
  String name;
  String lastname;
  String instagramAccount;
  int points;
  int credits;
  List<Contribution> contributions;

  Profile({
    this.id,
    this.createdAt,
    this.name,
    this.lastname,
    this.instagramAccount,
    this.points,
    this.credits,
    this.contributions
  });

  String getFullName() {
    return this.name + " " + this.lastname;
  }

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
    id: json["id"],
    createdAt: json["createdAt"],
    name: json["name"],
    lastname: json["lastname"],
    instagramAccount: json["instagramAccount"],
    points: json["points"],
    credits: json["credits"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "createdAt": createdAt,
    "name": name,
    "lastname": lastname,
    "instagramAccount": instagramAccount,
    "points": points,
    "credits": credits,
    "contribution": contributions.toString()
  };

}
