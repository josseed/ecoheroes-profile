import 'package:ecoheroes_profile/routes.dart';
import 'package:ecoheroes_profile/src/blocs/app_bloc.dart';
import 'package:ecoheroes_profile/src/theme/style.dart';
import 'package:flutter/material.dart';
import 'src/blocs/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();


  final appBloc = AppBloc();
  runApp(MyApp(appBloc));
}

class MyApp extends StatelessWidget {
  final AppBloc bloc;
  MyApp(this.bloc);

  @override
  Widget build(BuildContext context) {
    return Provider(
      bloc: bloc,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: appTheme(),
        title: 'Material App',
        initialRoute: 'profile',
        routes: routes
      )
    );
  }
}
