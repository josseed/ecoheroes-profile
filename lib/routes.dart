import 'package:ecoheroes_profile/src/ui/profile/profile_ui.dart';

import 'package:flutter/widgets.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder> {
  "profile": (BuildContext context) => ProfileUI(),
};
